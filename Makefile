SHELL = /bin/sh

PREFIX = /usr/local
EPREFIX = $(PREFIX)
BINDIR = $(EPREFIX)/bin

.PHONY: all
all: tick

tick: tick.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $<

.PHONY: install
install:
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f tick $(DESTDIR)$(BINDIR)

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(BINDIR)/tick

.PHONY: clean
clean:
	rm -f *.o tick

tick
====

tick is a simple clock for minimal window managers like i3 that don't have a
built-in clock, but instead display the root window name in the status bar.
Compared to the standard hack using date(1) and sleep(1), tick doesn't spawn
new processes on every update, and it uses an interval timer to tick exactly
once per second, regardless of how long the update code takes to run. To use
it, just compile the program and add it to your window manager configuration
file.

tick-stdout differs from [tick-x11](https://0xacab.org/daemonslayer/tick-x11) in
that it outputs the time to stdout rather than setting the X root window name.
This is suitable for i3 and sway, and probably others.

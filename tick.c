#include <err.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#define INTERVAL 1 // seconds

#define NITEMS(arr) (sizeof(arr) / sizeof(arr[0]))

static volatile sig_atomic_t keep_running = true;
static volatile sig_atomic_t has_ticked = true;

static void handle_sig(int sig)
{
	switch (sig) {
	case SIGALRM:
		has_ticked = true;
		break;
	case SIGINT:
	case SIGTERM:
		keep_running = false;
		break;
	}
}

static void set_time(void)
{
	time_t now;
	char *str;

	if (time(&now) == (time_t)-1) {
		warn("unable to get current time");
		return;
	}

	str = ctime(&now);
	if (str == NULL) {
		warn("unable to convert time to string");
		return;
	}

	(void)fputs(str, stdout);
}

int main(void)
{
	const int sigs[] = { SIGALRM, SIGINT, SIGTERM };
	struct sigaction sa;
	struct itimerval itv;

	sa.sa_handler = handle_sig;
	(void)sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	for (size_t i = 0; i < NITEMS(sigs); i++) {
		if (sigaction(sigs[i], &sa, NULL) == -1) {
			err(EXIT_FAILURE, "unable to install signal handler");
		}
	}

	itv.it_interval.tv_sec = INTERVAL;
	itv.it_interval.tv_usec = 0;
	itv.it_value.tv_sec = 1;
	itv.it_value.tv_usec = 0;
	if (setitimer(ITIMER_REAL, &itv, NULL) == -1) {
		err(EXIT_FAILURE, "unable to set interval timer");
	}

	setlinebuf(stdout);
	while (keep_running) {
		if (has_ticked) {
			set_time();
			has_ticked = false;
		}
		(void)pause();
	}

	return 0;
}
